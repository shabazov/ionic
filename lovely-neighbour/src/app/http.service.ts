import { Observable } from 'rxjs';
import { Neib } from './neib.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  readonly ROOT_URL;
  
  constructor(private http: HttpClient) { 
    this.ROOT_URL = 'http://localhost:3000';
  }

  getNeibs(): Observable<Neib[]> {
    return this.http.get<Neib[]>(`${this.ROOT_URL}/neibs`);
  };

  postNeib(payload: Neib): Observable<Neib> {
    return this.http.post<Neib>(`${this.ROOT_URL}/neibs`, payload);
  };


  deleteNeib(id: string) {
    return this.http.delete(`${this.ROOT_URL}/neibs/${id}`);
  };
}
