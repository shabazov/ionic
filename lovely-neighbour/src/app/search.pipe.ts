import { Neib } from './neib.interface';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(neibs: Neib[], search = ''): Neib[] {
    if(!search.trim()) {
      return neibs;
    };

    return neibs.filter((neib) => {
      return neib.name.toLowerCase().includes(search.toLowerCase())
    })
  }

}
