import { Neib } from './../neib.interface';
import { HttpService } from './../http.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  form: FormGroup;

  constructor(private fb: FormBuilder, private service: HttpService) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      country: ['', Validators.required],
      city: ['', Validators.required],
      street: ['', Validators.required],
      building: ['', Validators.required],
      zip: ['', Validators.required],
      description: ['', Validators.required]
    })
  }

  submit() {
    if(this.form.invalid) {
      return
    }
    const newNeib: Neib = {
      name: this.form.value.name,
      address: {
        country: this.form.value.country,
        city: this.form.value.city,
        street: this.form.value.street,
        building: this.form.value.building,
        zip: this.form.value.zip,
      },
      description: this.form.value.description,
    };

    this.service.postNeib(newNeib).subscribe(() => {
      console.log(newNeib);
    });
    this.form.reset();
    
  }
}
