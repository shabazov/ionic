export interface Neib {
  _id?: string;
  name: string;
  address: Object;
  description: String;
}