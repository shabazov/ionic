import { Neib } from './../neib.interface';
import { HttpService } from './../http.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  neibs: Neib[] = [];
  searchString = '';
  // selectedId: string;

  optionsObject = {
    slidesPerView: 1,
    grabCursor: true,
  }

  constructor(private httpService: HttpService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.loadNiebs();
  }

  onDelete(id: string) {
    this.httpService.deleteNeib(id).subscribe(() => this.loadNiebs());
  }

  loadNiebs() {
    this.httpService.getNeibs().subscribe((neibs) => {
      this.neibs = neibs;
    })
  }
}
