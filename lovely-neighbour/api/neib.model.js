const mongoose = require('mongoose');

const NeibSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 2,
    trim: true
  },
  address: {
    country: String,
    city: String,
    street: String,
    building: String,
    zip: String
  },
  description: {
    type: String,
    required: true,
    trim: true
  }
});

const Neib = mongoose.model('Neib', NeibSchema);

module.exports = { Neib };