const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const { Neib } = require('./neib.model');

const { mongoose } = require('./mongoose');

//////////////////////////////////////////////////
app.use(cors());
app.use(bodyParser.json());

app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//////////////////////////////////////////////////



app.get('/neibs', (req, res) => {
  Neib.find({}).then((neibs) => {
    res.send(neibs)
  })
});

app.post('/neibs', (req, res) => {
  let name = req.body.name;
  let address = {};
  address.country = req.body.address.country;
  address.city = req.body.address.city;
  address.street = req.body.address.street;
  address.building = req.body.address.building;
  address.zip = req.body.address.zip;
  let description = req.body.description;
  let newNeib = new Neib({
    name,
    address,
    description
  });
  newNeib.save().then((neibDoc) => {
    res.send(neibDoc)
  })

});

app.delete('/neibs/:id', (req, res) => {
  Neib.findOneAndRemove({
    _id: req.params.id
  }).then((removedDoc) => {
    res.send(removedDoc)
  });
});






//////////////////////////////////////////////////

app.listen(3000, () => {
  console.log('server is listening on port 3000');
});