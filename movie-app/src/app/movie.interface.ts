export interface Movie {
  name: string;
  rate: number;
  poster: string;
  genre: string;
}