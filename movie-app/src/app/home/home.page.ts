import { MoviesService } from './../movies.service';
import { Movie } from './../movie.interface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  movies: Movie[] = [];

  options = {
    loop: true,
    centeredSlides: true,
    spaceBetween: -70
  }

  constructor(private moviesService: MoviesService) {}

  ngOnInit() {
    this.movies = this.moviesService.getMovies();
  }

  genreChange(genre: string) {
    console.log(genre);
    

    if(genre === 'all') {
      this.movies = this.moviesService.getMovies();
    }
    else {
      this.movies = this.movies.filter(movie => {
        return movie.genre.toLowerCase().includes(genre.toLowerCase());
      })
    }
  }

}
