import { Movie } from './movie.interface';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  movies: Movie[] = [
    {
      name: 'Top Gun',
      genre: 'action',
      rate: 9.8,
      poster: 'https://www.avpasion.com/wp-content/uploads/2019/12/poster-oficial-top-gun-maverick-2020.jpg'
    },
    {
      name: 'Top Gun: Meverick',
      genre: 'action',
      rate: 9.6,
      poster: 'https://images-na.ssl-images-amazon.com/images/I/619z3KIhqbL._AC_SY679_.jpg'
    },
    {
      name: 'Godzilla VS. Kong',
      genre: 'thriller',
      rate: 9.1,
      poster: 'https://nosomosnonos.com/wp-content/uploads/2019/12/IMG_20191227_135628.jpg'
    },
    {
      name: 'We Are The Millers',
      genre: 'comedy',
      rate: 9.3,
      poster: 'https://i.pinimg.com/originals/91/cc/53/91cc5305138adf10f7437fcb361c8a47.jpg'
    },
    {
      name: 'Avengers. Age of Altron',
      genre: 'action',
      rate: 9.6,
      poster: 'https://sm.ign.com/ign_es/screenshot/default/avengers2-poster2_eh1g.jpg'
    },
    {
      name: 'Spiderman',
      genre: 'action',
      rate: 9.6,
      poster: 'https://media.metrolatam.com/2019/05/22/smfh1-c681bbff322027afa5d3473d440e20d1-0x1200.jpg'
    },

  ];

  constructor() { }

  getMovies() {
    return this.movies;
  }
}
