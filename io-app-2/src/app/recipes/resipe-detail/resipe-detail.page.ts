import { Recipe } from './../recipe.model';
import { RecipesService } from './../recipes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-resipe-detail',
  templateUrl: './resipe-detail.page.html',
  styleUrls: ['./resipe-detail.page.scss'],
})
export class ResipeDetailPage implements OnInit {

  loadedRecipe: Recipe;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private recipesService: RecipesService,
    private router: Router,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if(!paramMap.has('recipeId')) {
        // redirect
        this.router.navigate(['/recipes']);
        return;
      }
      const recipeId = paramMap.get('recipeId');
      this.loadedRecipe = this.recipesService.getRecipe(recipeId);
    })
  };

  deleteRecipe() {
    this.alertCtrl.create({
      header: 'are you sure?',
      message: 'Delete recipe',
      buttons: [{
        text: 'cancel',
        role: 'cancel'
      }, {
        text: 'delete',
        role: 'confirm',
        handler: () => {
          this.recipesService.deleteRecipe(this.loadedRecipe.id);
          this.router.navigate(['/recipes']);
        }
      }]
    }).then(alertElement => {
      alertElement.present();
    });
  }

}
