import { Recipe } from './recipe.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {

  private recipes: Recipe[] = [
    {
      id: 'recipe1',
      title: 'copos de avena',
      imageUrl: 'https://statics-cuidateplus.marca.com/sites/default/files/styles/natural/public/avena-copos.jpg?itok=K28-AWk4',
      ingredients: [
        'copos de avena',
        'maggi',
        'tomatos',
        'mixed nuts'
      ]
    },
    {
      id: 'recipe2',
      title: 'fried eggs',
      imageUrl: 'https://www.simplyrecipes.com/wp-content/uploads/2018/12/Fry-an-Egg-METHOD-3.jpg',
      ingredients: [
        'eggs',
        'cheese',
        'pees',
        'bread'
      ]
    }
  ];

  constructor() { }

  getAllRecipes() {
    return [...this.recipes];
  };

  getRecipe(recipeId: string) {
    return {...this.recipes.find(recipe => { return recipe.id === recipeId })};
  };

  deleteRecipe(recipeId: string) {
    this.recipes = this.recipes.filter(recipe => {
      return recipe.id !== recipeId;
    })
    console.log(this.recipes);
    
  }
}
